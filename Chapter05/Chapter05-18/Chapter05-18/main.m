//
//  main.m
//  Chapter05-18
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int i = 0;  // 定义一个循环计数变量
    start:
        NSLog(@"i: %d", i);
        i++;
        if (i < 10) {   // 如果i小于10，再次跳转到start标签处
            goto start;
        }
    }
    return 0;
}
