//
//  main.m
//  Chapter05-04
//
//  Created by QinTuanye on 2018/7/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int n, triangularNumber;
        triangularNumber = 0;
        for (n = 1; n <= 200; n = n + 1) {
            triangularNumber += n;
        }
        NSLog(@"The 200th triangular number is %i", triangularNumber);
    }
    return 0;
}
