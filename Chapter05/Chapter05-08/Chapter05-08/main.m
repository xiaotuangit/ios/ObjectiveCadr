//
//  main.m
//  Chapter05-08
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 外层循环
        for (int i = 0; i < 5; i++) {
            // 内层循环
            for (int j = 0; j < 3; j++) {
                NSLog(@"i的值为: %d, j的值为：%d", i, j);
            }
        }
    }
    return 0;
}
