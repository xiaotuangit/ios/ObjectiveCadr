//
//  main.m
//  Chapter05-20
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        for (int i = 0; i < 5; i++) {   // 外层循环
            for (int j = 0; j < 3; j++) {   // 内层循环
                NSLog(@"i的值为：%d, j的值为：%d", i, j);
                if (j >= 1) {
                    goto outer; // 跳到outer标签处
                }
            }
        outer:; // 标签后的分号代表一条空语句
        }
        NSLog(@"循环结束");
    }
    return 0;
}
