//
//  main.m
//  Chapter05-01
//
//  Created by QinTuanye on 2018/7/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int triangularNumber;
        triangularNumber = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8;
        NSLog(@"The xiaoqiu number is %i", triangularNumber);
    }
    return 0;
}
