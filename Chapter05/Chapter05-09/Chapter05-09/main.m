//
//  main.m
//  Chapter05-09
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int count = 1;
        while (count <= 5) {
            NSLog(@"%i", count);
            ++count;
        }
    }
    return 0;
}
