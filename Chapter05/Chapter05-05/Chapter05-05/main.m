//
//  main.m
//  Chapter05-05
//
//  Created by QinTuanye on 2018/7/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int n, triangularNumber;
        NSLog(@"TABLE OF TRIANGULAR NUMBERS");
        NSLog(@"1 to n");
        NSLog(@"--  --------");
        triangularNumber = 0;
        for (n = 1; n <= 10; ++n) {
            triangularNumber += n;
            NSLog(@"%2i  %8i", n, triangularNumber);
        }
    }
    return 0;
}
