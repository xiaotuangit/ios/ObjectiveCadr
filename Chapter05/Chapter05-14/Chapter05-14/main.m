//
//  main.m
//  Chapter05-14
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int count = 1;  // 定义变量count
        // 执行do while循环
        do {
            NSLog(@"count: %d", count);
            count++;    // 循环迭代语句
            // 循环条件后紧跟while关键字
        } while (count < 10);
        NSLog(@"循环结束!");
        
        int count2 = 20;    // 定义变量count2
        // 执行do while循环
        do
            // 这行代码把循环体和迭代部分合并成了一行代码
            NSLog(@"count2: %d", count2++);
        while (count2 < 10);
        NSLog(@"循环结束!");
    }
    return 0;
}
