//
//  main.m
//  Chapter05-16
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 一个简单的for循环
        for (int i = 0; i < 10; i++) {
            NSLog(@"i的值是：%d", i);
            if (i == 2) {
                // 执行该语句时将结束循环
                break;
            }
        }
    }
    return 0;
}
