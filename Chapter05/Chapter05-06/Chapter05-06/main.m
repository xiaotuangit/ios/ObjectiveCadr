//
//  main.m
//  Chapter05-06
//
//  Created by QinTuanye on 2018/7/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int n, number, triangularNumber;
        NSLog(@"number do you want?");
        scanf("%i", &number);
        triangularNumber = 0;
        for (n = 1; n <= number; ++n) {
            triangularNumber += n;
        }
        NSLog(@"Triangular number %i is %i\n", number, triangularNumber);
    }
    return 0;
}
