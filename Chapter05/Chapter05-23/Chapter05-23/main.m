//
//  main.m
//  Chapter05-23
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int p, d;
        BOOL isPrime;
        for (p = 2; p <= 50; ++p) {
            isPrime = YES;
            for (d = 2; d < p; ++d) {
                if (p %d == 0) {
                    isPrime = NO;
                }
            }
            if (isPrime == YES) {
                NSLog(@"%i ", p);
            }
        }
    }
    return 0;
}
