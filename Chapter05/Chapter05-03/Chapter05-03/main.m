//
//  main.m
//  Chapter05-03
//
//  Created by QinTuanye on 2018/7/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 同时定义了3个初始化变量，使用&&来组合多个逻辑表达式
        for (int b = 0, s = 0, p = 0; b < 10 && s < 4 && p < 10; p++) {
            NSLog(@"b: %d", b++);
            NSLog(@"s: %d, p: %d", ++s, p);
        }
    }
    return 0;
}
