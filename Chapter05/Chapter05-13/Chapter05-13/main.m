//
//  main.m
//  Chapter05-13
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int count = 0;
        while (count < 10);
        {
            NSLog(@"count: %d", count);
            count++;
        }
    }
    return 0;
}
