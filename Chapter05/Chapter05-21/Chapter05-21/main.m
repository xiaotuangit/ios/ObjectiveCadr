//
//  main.m
//  Chapter05-21
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        for (int i = 0; i < 3; i++) {   // 一个简单的for循环
            for (int j = 0; j < 5; j++) {
                NSLog(@"i: %d, j: %d", i, j);
                if (j >= 2) {
                    return 0;
                }
            }
        }
        NSLog(@"循环后的语句");
    }
    return 0;
}
