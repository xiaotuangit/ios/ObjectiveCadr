//
//  main.m
//  Chapter05-12
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int count = 0;  // 循环的初始条件
        while (count < 10)  {  // 当count小于10时，执行循环体
            NSLog(@"count:%d", count);
            count++;    // 迭代语句
        }
        NSLog(@"循环结束!");
        
        // 下面是一个死循环
        int count2 = 0;
        while (count2 < 10) {
            NSLog(@"不停执行的死循环 %d ", count2);
            count --;
        }
        NSLog(@"永远无法跳出的循环体");
    }
    return 0;
}
