//
//  main.m
//  Chapter05-02
//
//  Created by QinTuanye on 2018/7/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 循环的初始化条件，循环条件，循环迭代语句都在下面一行
        for (int count = 0; count < 10; count++) {
            NSLog(@"count, %d", count);
        }
        NSLog(@"循环结束!");
    }
    return 0;
}
