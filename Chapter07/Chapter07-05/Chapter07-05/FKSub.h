//
//  FKSub.h
//  Chapter07-05
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "FKParent.h"

@interface FKSub : FKParent
- (void)accessOwner;
@end
