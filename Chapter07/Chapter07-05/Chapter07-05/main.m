//
//  main.m
//  Chapter07-05
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FKSub.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        FKSub *sub = [[FKSub alloc]init];
        [sub accessOwner];
    }
    return 0;
}
