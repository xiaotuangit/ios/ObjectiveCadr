//
//  FKParent.m
//  Chapter07-05
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "FKParent.h"

@implementation FKParent

@synthesize _a;

- (id)init {
    if (self = [super init]) {
        self->_a = 5;
    }
    return self;
}
@end
