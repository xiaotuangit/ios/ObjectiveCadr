//
//  Apple.h
//  Chapter07-01
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Fruit.h"

@interface Apple : Fruit

@end
