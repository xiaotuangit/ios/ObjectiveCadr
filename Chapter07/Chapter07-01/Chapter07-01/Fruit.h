//
//  Fruit.h
//  Chapter07-01
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fruit : NSObject
@property (nonatomic, assign) double weight;
- (void)info;
@end
