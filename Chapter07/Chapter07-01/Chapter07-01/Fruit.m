//
//  Fruit.m
//  Chapter07-01
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "Fruit.h"

@implementation Fruit

- (void)info {
    NSLog(@"我是多汁的水果！体重%gg!", self.weight);
}
@end
