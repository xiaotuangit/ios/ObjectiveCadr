//
//  main.m
//  Chapter07-01
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Apple.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 创建Apple的对象
        Apple *a = [[Apple alloc]init];
        // Apple对象本身没有weight属性
        // 因为Apple的父类有weight属性，也可以访问Apple对象的weight属性
        a.weight = 56;
        // 调用Apple对象的info方法
        [a info];
    }
    return 0;
}
