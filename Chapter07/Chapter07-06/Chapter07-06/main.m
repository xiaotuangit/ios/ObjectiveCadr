//
//  main.m
//  Chapter07-06
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClassA: NSObject
{
    int x;
}

- (void)initVar;
@end

@implementation ClassA

- (void)initVar {
    x = 100;
}
@end

@interface ClassB: ClassA
{
    int y;
}

- (void)initVar;
- (void)printVar;
@end

@implementation ClassB

- (void)initVar {
    x = 200;
    y = 300;
}

- (void)printVar {
    NSLog(@"x = %i", x);
    NSLog(@"y = %i", y);
}
@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        ClassB *b = [[ClassB alloc]init];
        [b initVar];    // 覆盖ClassA的方法
        [b printVar];   // 显示x和y的值
    }
    return 0;
}
