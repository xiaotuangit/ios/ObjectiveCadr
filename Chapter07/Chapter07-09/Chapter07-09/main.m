//
//  main.m
//  Chapter07-09
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NSStringUtilities.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSString *string1 = @"http://www.toppr.net/";
        NSString *string2 = @"guanxijing";
        
        if ([string1 isURL]) {
            NSLog(@"string1 is a URL");
        } else {
            NSLog(@"string1 is not a URL");
        }
        
        if ([string2 isURL]) {
            NSLog(@"string2 is a URL");
        } else {
            NSLog(@"string2 is not a URL");
        }
    }
    return 0;
}
