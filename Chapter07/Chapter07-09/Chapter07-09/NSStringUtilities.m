//
//  NSStringUtilities.m
//  Chapter07-09
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "NSStringUtilities.h"

@implementation NSString (NSStringUtilities)

- (BOOL)isURL {
    if ([self hasPrefix:@"http://"]) {
        return YES;
    } else {
        return NO;
    }
}

@end
