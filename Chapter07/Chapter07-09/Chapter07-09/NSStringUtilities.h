//
//  NSStringUtilities.h
//  Chapter07-09
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSStringUtilities)
- (BOOL)isURL;
@end
