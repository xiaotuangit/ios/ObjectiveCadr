//
//  main.m
//  Chapter07-07
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Test.h"
#import "Test1.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        id idTest;
        Test *test = [[Test alloc]init];
        Test1 *test1 = [[Test1 alloc]init];
        idTest = test;
        [idTest print];
        idTest = test1;
        [idTest print];
    }
    return 0;
}
