//
//  main.m
//  Chapter07-08
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ClassB.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        ClassA *classA = [[ClassA alloc]init];
        ClassB *classB = [[ClassB alloc]init];
        classA->x = 1;
        NSLog(@"%i", classA->x);
        [classB print];
//        classB->z = 3.0;
//        NSLog(@"%e", classA->z);
    }
    return 0;
}
