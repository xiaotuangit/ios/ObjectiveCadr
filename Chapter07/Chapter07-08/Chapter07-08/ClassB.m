//
//  ClassB.m
//  Chapter07-08
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "ClassB.h"

@implementation ClassB

- (void)print {
    y = 2.0f;
    NSLog(@"%f", y);
}
@end
