//
//  ClassA.h
//  Chapter07-08
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClassA : NSObject
{
    @public
    int x;
    @protected
    float y;
    @private
    double z;
}
@end
