//
//  Archaeopteryx.h
//  Chapter07-03
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "Bird.h"

@interface Archaeopteryx : Bird
- (void)callOverridedMethod;
@end
