//
//  Bird.m
//  Chapter07-03
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "Bird.h"

@implementation Bird

// Bird类的fly方法
- (void)fly {
    NSLog(@"我会飞...");
}
@end
