//
//  Archaeopteryx.m
//  Chapter07-03
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "Archaeopteryx.h"

@implementation Archaeopteryx
// 重写父类的fly方法
- (void)fly {
    NSLog(@"我是鸟类的祖宗...");
}

- (void)callOverridedMethod {
    // 在子类方法中通过super显示调用父类被覆盖的实例方法
    [super fly];
}
@end
