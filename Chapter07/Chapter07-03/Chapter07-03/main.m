//
//  main.m
//  Chapter07-03
//
//  Created by QinTuanye on 2018/7/31.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Archaeopteryx.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 创建Archaeopteryx对象
        Archaeopteryx *os = [[Archaeopteryx alloc]init];
        // 执行Archaeopteryx对象的fly方法，将输出“我是鸟类的祖宗...”
        [os fly];
        [os callOverridedMethod];
    }
    return 0;
}
