//
//  main.m
//  Chapter02-09
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int sum;
        sum = 10 + 20;
        NSLog(@"10和20的和是： %i", sum);
    }
    return 0;
}
