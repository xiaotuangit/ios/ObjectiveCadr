//
//  main.m
//  Chapter02-06
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        char a = 120;
        char b = 121;
        NSLog(@"%c, %c", a, b);
        NSLog(@"%i, %i", a, b);
    }
    return 0;
}
