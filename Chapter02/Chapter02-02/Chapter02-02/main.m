//
//  main.m
//  Chapter02-02
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int a, b, c, d;
        unsigned u;
        a = 12;
        b = -24;
        u = 10;
        c = a + u;
        d = b + u;
        NSLog(@"a+u=%i,b+u=%i", c, d);
    }
    return 0;
}
