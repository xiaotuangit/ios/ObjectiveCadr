//
//  main.m
//  Chapter02-07
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int a = 56;
        NSLog(@"===%d===", a);
        NSLog(@"===%9d===", a); // 输出整数占9位
        NSLog(@"===%-9d===", a);    // 输出整数占9位，并且左对齐
        NSLog(@"===%o===", a);  // 输出八进制数
        NSLog(@"===%x===", a);  // 输出十六进制数
        long b = 12;
        NSLog(@"===%ld===", b); // 输出lonng int型的整数
        NSLog(@"===%lx===", b); // 以十六进制输出long int 型的整数
        double dl = 2.3;
        NSLog(@"===%f===", dl); // 以小数形式输出浮点数
        NSLog(@"===%e===", dl); // 以指数形式输出浮点数
        NSLog(@"===%g===", dl); // 以最简形式输出浮点数
        // 以小数形式输出浮点数，并且最少占用9位
        NSLog(@"===%9f===", dl);    // 以小数形式输出浮点数，至少占用9位，小数部分共4位
        NSLog(@"===%9.4f===", dl);
        long double d2 = 2.3;
        NSLog(@"===%Lf===", d2);    // 以小数形式输出长浮点数
        NSLog(@"===%Le===", d2);    //以指数形式输出长浮点数
        NSLog(@"===%Lg===", d2);    // 以最简形式输出长浮点数
        // 以小数形式输出长浮点数，至少占用9位
        NSLog(@"===%9Lf===", d2);
        // 以小数形式输出长浮点数，至少占用9位，小数部分共4位
        NSLog(@"===%9.4Lf===", d2);
        NSString *str = @"iOS好好学";
        NSLog(@"===%@===", str);    // 输出Objective-C的字符串
        NSDate *date = [NSDate date];
        NSLog(@"===%@===", date);   // 输出Objective-C对象
    }
    return 0;
}
