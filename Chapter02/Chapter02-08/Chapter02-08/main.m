//
//  main.m
//  Chapter02-08
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int a = 3, b, c = 5;
        b = a + c;
        NSLog(@"a=%i, b=%i, c=%i", a, b, c);
    }
    return 0;
}
