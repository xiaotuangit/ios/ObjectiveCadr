//
//  main.m
//  Chapter02-04
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        float a = 123456.789e5;
        float b = a + 20;
        NSLog(@"%f", a);
        NSLog(@"%f", b);
    }
    return 0;
}
