//
//  main.m
//  Chapter06-07
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FKPerson.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        FKPerson *p = [[FKPerson alloc]init];
        // 因为_age成员变量已被隐藏，所以下面语句将出现编译错误
        // p->_age = 1000;
        // 下面语句编译不会出现错误，但运行时将提示设置的年龄不合法
        // 程序不会修改p的_age成员变量
        [p setAge:1000];
        // 访问p的_age成员变量也必须通过其对应的getter方法
        // 因为上面从未成功设置p的_age成员变量，故此处输出0
        NSLog(@"当我们没有设置age成员变量是：%d", [p age]);
        // 成功修改p的_age成员变量
        [p setAge:30];
        // 因为上面成功设置了p的_age成员变量，故下面输出30
        NSLog(@"当我们成功设置_age成员变量后：%d", [p age]);
        // 不能直接操作p的_name成员变量，只能通过其对应的setter方法
        // 因为“李刚”字符串长度满足2~6，所以科研成功设置
        [p setName:@"管玉龙"];
        NSLog(@"当我们成功设置_name成员变量后：%@", [p name]);
    }
    return 0;
}
