//
//  KCard.h
//  Chapter06-08
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KCard : NSObject
// 使用@property定义两个property
@property (nonatomic, copy) NSString *flower;
@property (nonatomic, copy) NSString *value;
@end
