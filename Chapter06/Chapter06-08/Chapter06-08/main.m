//
//  main.m
//  Chapter06-08
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KCard.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        KCard *card = [[KCard alloc]init];
        // 通过点运算符对属性赋值
        card.flower = @"♠";
        card.value = @"A";
        // 通过点运算符来访问属性值
        NSLog(@"赌神的底牌为：%@%@", card.flower, card.value);
    }
    return 0;
}
