//
//  main.m
//  Chapter06-05
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction:NSObject
{
    int numerator;
    int denominator;
}

- (void)print;
- (void)setNumerator:(int)n;
- (void)setDenominator:(int)d;
- (int)numerator;
- (int)denominator;
@end

// ------- @implementation部分 --------
@implementation Fraction

- (void)print {
    NSLog(@"%i/%i", numerator, denominator);
}

- (void)setNumerator:(int)n {
    numerator = n;
}

- (void)setDenominator:(int)d {
    denominator = d;
}

- (int)numerator {
    return numerator;
}

- (int)denominator {
    return denominator;
}
@end

// -------- 程序片段 --------
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Fraction *myFraction = [[Fraction alloc]init];
        // 分数 2/3
        [myFraction setNumerator:2];
        [myFraction setDenominator:3];
        // 使用显示分数的两种新方法
        NSLog(@"The value of myFraction is: %i/%i", [myFraction numerator], [myFraction denominator]);
    }
    return 0;
}
