//
//  Fraction.m
//  Chapter06-09
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "Fraction.h"

@implementation Fraction

@synthesize numerator, denominator;

- (void)print {
    NSLog(@"%i/%i", numerator, denominator);
}

- (double)convertToNum {
    if (denominator != 0) {
        return (double)numerator / denominator;
    } else {
        return 1.0;
    }
}

- (void)setTo:(int)n over:(int)d {
    numerator = n;
    denominator = d;
}
@end
