//
//  Fraction.h
//  Chapter06-09
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject
{
    int numerator;
    int denominator;
}

@property int numerator, denominator;

- (void)print;
- (void)setTo:(int)n over:(int)d;
- (double)convertToNum;
@end
