//
//  main.m
//  Chapter06-01
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int numberator = 2;
        int denominator = 3;
        NSLog(@"The fraction is %i/%i", numberator, denominator);
    }
    return 0;
}
