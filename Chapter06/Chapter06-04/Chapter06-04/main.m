//
//  main.m
//  Chapter06-04
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction: NSObject{
    int numerator;
    int denominator;
}

- (void)print;
- (void)setNumberator:(int)n;
- (void)setDenominator:(int)d;
@end

// ------- @实现部分 -------
@implementation Fraction

- (void)print {
    NSLog(@"%i/%i", numerator, denominator);
}

- (void)setNumberator:(int)n {
    numerator = n;
}

- (void)setDenominator:(int)d {
    denominator = d;
}
@end

// -------- 程序段 --------
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Fraction *frac1 = [[Fraction alloc]init];
        Fraction *frac2 = [[Fraction alloc]init];
        // 2/3
        [frac1 setNumberator:2];
        [frac1 setDenominator:3];
        // 3/7
        [frac2 setNumberator: 3];
        [frac2 setDenominator:7];
        // 输出
        NSLog(@"First fraction is: ");
        [frac1 print];
        NSLog(@"Second fraction is: ");
        [frac2 print];
    }
    return 0;
}
