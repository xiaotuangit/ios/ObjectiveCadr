//
//  Fraction.h
//  Chapter06-06
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject
{
    int numerator;
    int denominator;
}
- (void)print;
- (void)setNumerator:(int)n;
- (void)setDenominator:(int)d;
- (int)numerator;
- (int)denominator;
- (double)convertToNum;
@end
