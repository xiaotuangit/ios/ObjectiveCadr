//
//  main.m
//  Chapter06-06
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Fraction.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Fraction *myFraction = [[Fraction alloc]init];
        // 1/3
        [myFraction setNumerator:2];
        [myFraction setDenominator:3];
        NSLog(@"The value of myFraction is: ");
        [myFraction print];
    }
    return 0;
}
