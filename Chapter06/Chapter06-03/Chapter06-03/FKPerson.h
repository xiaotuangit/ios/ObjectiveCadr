//
//  FKPerson.h
//  Chapter06-03
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FKPerson : NSObject
{
    // 定义两个成员变量
    NSString *_name;
    int _age;
}

// 定义一个setName； andAge:方法
- (void)setName:(NSString *)name andAge:(int)age;
// 定义一个say:方法，并不提供实现
- (void)say:(NSString *)content;
// 定义一个不带形参的info方法
- (NSString *)info;
// 定义一个类方法
+ (void)foo;
@end
