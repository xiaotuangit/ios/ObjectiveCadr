//
//  main.m
//  Chapter06-02
//
//  Created by QinTuanye on 2018/7/30.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

// ------- @interface定义部分 --------
@interface Fraction: NSObject
{
    int numerator;
    int denominator;
}

- (void)print;
- (void)setNumerator: (int)n;
- (void)setDenominator: (int)d;
@end

// ------- @implementation实现部分 -------
@implementation Fraction

- (void)print {
    NSLog(@"%i/%i", numerator, denominator);
}

- (void)setNumerator: (int)n {
    numerator = n;
}

- (void)setDenominator:(int)d {
    denominator = d;
}
@end

// ------- Program程序段，实现具体功能 -------
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Fraction *myFraction;
        // 创建一个实例
        myFraction = [Fraction alloc];
        myFraction = [myFraction init];
        
        // 设置为2/3
        [myFraction setNumerator:2];
        [myFraction setDenominator:3];
        
        // 打印输出
        NSLog(@"The value of myFraction is: ");
        [myFraction print];
    }
    return 0;
}
