//
//  Complex.m
//  Chapter08-02
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "Complex.h"

@implementation Complex

@synthesize real, imaginary;

- (void)print {
    NSLog(@"%g + %gi", real, imaginary);
}

- (void)setReal:(double)a andImaginary:(double)b {
    real = a;
    imaginary = b;
}

- (Complex *)add:(Complex *)f {
    Complex *result = [[Complex alloc]init];
    [result setReal:real + [f real] andImaginary:imaginary + [f imaginary]];
    return result;
}
@end
