//
//  main.m
//  Chapter08-05
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Complex.h"
#import "Fraction.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        id dataValue;
        Fraction *f1 = [[Fraction alloc]init];
        Complex *c1 = [[Complex alloc]init];
        
        [f1 setTo:2 over:5];
        [c1 setReal:10.0 andImaginary:2.5];
        
        // 第一种情况输出：2/5
        
        dataValue = f1;
        [dataValue print];
        
        // 第二种情况输出：10 + 2.5i
        
        dataValue = c1;
        [dataValue print];
    }
    return 0;
}
