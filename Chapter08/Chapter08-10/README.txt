@finally块中的return语句使@try语句中的return语句失效

最好不要在@finally块中使用return语句，否则将会使在@try块的return语句失效。