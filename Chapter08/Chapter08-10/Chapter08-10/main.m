//
//  main.m
//  Chapter08-10
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

BOOL test() {
    @try {
        // 因为finally块中包含了return语句，
        // 所以下面的return语句失去作用
        return YES;
    }
    @finally {
        return NO;
    }
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        BOOL a = test();
        NSLog(@"%d", a);    // 输出代表NO的0
    }
    return 0;
}
