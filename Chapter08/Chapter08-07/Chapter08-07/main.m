//
//  main.m
//  Chapter08-07
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Square.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Square *mySquare = [[Square alloc]init];
        if ([mySquare isMemberOfClass:[Square class]] == YES) {
            NSLog(@"mySquare is a member of Square class");
        }
        if ([mySquare isMemberOfClass:[Rectangle class]] == YES) {
            NSLog(@"mySquare is a member of Rectangle class");
        }
        if ([mySquare isMemberOfClass:[NSObject class]] == YES) {
            NSLog(@"mySquare is a member of NSObject class");
        }
        if ([mySquare isKindOfClass:[Square class]] == YES) {
            NSLog(@"mySquare is kind of Square");
        }
        if ([mySquare isKindOfClass:[Rectangle class]] == YES) {
            NSLog(@"mySquare is a kind of Rectangle");
        }
        if ([mySquare isKindOfClass:[NSObject class]] == YES) {
            NSLog(@"mySquare is a kind of NSObject");
        }
        if ([mySquare respondsToSelector:@selector(setSide:)] == YES) {
            NSLog(@"mySquare responds to setSide: method");
        }
        if ([mySquare respondsToSelector:@selector(setWith:andHeight:)] == YES) {
            NSLog(@"mSquare responds to setWidth:andHeight: method");
        }
        if ([Square respondsToSelector:@selector(alloc)] == YES) {
            NSLog(@"Square class responds to alloc method");
        }
        if ([Rectangle instanceMethodForSelector:@selector(setSide:)] == YES) {
            NSLog(@"Instances of Rectangle respond to setSide: method");
        }
        if ([Square instancesRespondToSelector:@selector(setSide:)] == YES) {
            NSLog(@"Instances of Square respond to setSide: method");
        }
        if ([Square isSubclassOfClass:[Rectangle class]] == YES) {
            NSLog(@"Square is a subclass of a rectangle");
        }
    }
    return 0;
}
