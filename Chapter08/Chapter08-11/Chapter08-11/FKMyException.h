//
//  FKMyException.h
//  Chapter08-11
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

// 定义类的接口部分
@interface FKMyException : NSException

@end
