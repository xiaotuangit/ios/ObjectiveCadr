//
//  FKDog.m
//  Chapter08-11
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "FKDog.h"
#import "FKMyException.h"

// 为FKDog提供实现部分
@implementation FKDog

- (void)setAge:(int)age {
    if (self.age != age) {
        // 检查年龄是否在0~15
        if (age > 15 || age < 0) {
            // 手动抛出异常
            @throw [[FKMyException alloc]initWithName:@"IllegalArgumentException" reason:@"狗的年龄必须在0~15之间" userInfo:nil];
        }
        _age = age;
    }
}
@end
