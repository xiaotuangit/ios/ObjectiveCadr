//
//  main.m
//  Chapter08-11
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FKDog.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 创建FKDog对象
        FKDog *dog = [[FKDog alloc]init];
        dog.age = 20;
        NSLog(@"狗的年龄为：%d", dog.age);
        dog.age = 80;
    }
    return 0;
}
