//
//  FKBase.m
//  Chapter08-01
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "FKBase.h"

@implementation FKBase

- (void)base {
    NSLog(@"这是父类中的普通base方法");
}

- (void)test {
    NSLog(@"这是父类中的将被覆盖的test方法");
}
@end
