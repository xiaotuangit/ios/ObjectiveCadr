//
//  FKSubclass.m
//  Chapter08-01
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "FKSubclass.h"

@implementation FKSubclass

- (void)test {
    NSLog(@"子类中的覆盖父类中的test方法");
}

- (void)sub {
    NSLog(@"这是子类中的sub方法");
}
@end
