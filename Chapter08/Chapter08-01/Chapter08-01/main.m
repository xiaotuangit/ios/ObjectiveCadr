//
//  main.m
//  Chapter08-01
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FKSubclass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 编译时类型和运动时类型完全一样，因此不存在多态性
        FKBase *bc = [[FKBase alloc]init];
        // 两次调用将执行FKBase的方法
        [bc base];
        [bc test];
        // 编译时类型和运行时类型完全一样，因此不存在多态性
        FKSubclass *sc = [[FKSubclass alloc]init];
        // 调用将执行从父类继承到的base方法
        [sc base];
        // 调用将执行子类重写的test方法
        [sc test];
        // 调用将执行子类定义的sub方法
        [sc sub];
        // 编译时类型和运行时类型不一样，多态发生
        FKBase *ploymophicBc = [[FKSubclass alloc]init];
        // 调用将执行从父类继承到的base方法
        [ploymophicBc base];
        // 调用将执行子类重写的test方法
        [ploymophicBc test];
        // 因为ploymophicBc的编译类型是FKBase
        // FKBase类没有提供sub方法，所以下面代码编译时会出现错误。
        // [ploymophicBc sub];
        // 可以将任何类型的指针变量赋值给id类型的变量
        id dyna = ploymophicBc;
        [dyna sub];
    }
    return 0;
}
