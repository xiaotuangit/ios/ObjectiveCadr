//
//  FKSubclass.h
//  Chapter08-01
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "FKBase.h"

@interface FKSubclass : FKBase
- (void)sub;
@end
