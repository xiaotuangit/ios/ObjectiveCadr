//
//  FKEatable.h
//  Chapter08-09
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

// 定义协议
@protocol FKEatable
@optional
- (void)teste;
@end
