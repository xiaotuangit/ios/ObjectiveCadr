//
//  FKApple.h
//  Chapter08-09
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FKEatable.h"

// 定义类的接口部分，实现FKEatable协议
@interface FKApple : NSObject <FKEatable>

@end
