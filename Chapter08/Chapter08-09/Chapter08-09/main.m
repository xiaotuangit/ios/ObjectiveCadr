//
//  main.m
//  Chapter08-09
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FKApple.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        @try {
            FKApple *app = [[FKApple alloc]init];   // 创建FKApple对象
            [app teste];    // 调用taste方法
        }
        @catch (NSException *exception) {
            NSLog(@"==捕捉异常==");
            NSLog(@"捕捉异常：%@, %@", exception.name, exception.reason);
        }
        @finally {
            // 此处可进行资源回收等操作
            NSLog(@"资源回收!");
        }
        NSLog(@"程序执行完成!");
    }
    return 0;
}
