//
//  main.m
//  Chapter08-04
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 声明hello时使用NSObject类，则hello的编译时类型是NSObject,
        // NSObject是所有类的父类，但hello变量的实际类型是NSString
        NSObject *hello = @"Hello";
        // 使用isKindOfClass判断该变量所指的对象是否为指定类或其子类的实例
        NSLog(@"字符串是否是NSObject类的实例：%d", ([hello isKindOfClass:[NSObject class]]));
        // 返回YES
        NSLog(@"字符串是否是NSString类的实例：%d", ([hello isKindOfClass:[NSString class]]));
        // 返回NO
        NSLog(@"字符串是否是NSDate类的实例：%d", ([hello isKindOfClass:[NSDate class]]));
        NSString *a = @"Hello";
        // 返回NO
        NSLog(@"a是否是NSDate类的实例：%d", ([a isKindOfClass:[NSDate class]]));
    }
    return 0;
}
