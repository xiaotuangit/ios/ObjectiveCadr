//
//  main.m
//  Chapter08-08
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Fraction.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Fraction *f = [[Fraction alloc]init];
        @try {
            [f noSuchMethod];
        }
        @catch (NSException *exception) {
            NSLog(@"Caught %@: %@", [exception name], [exception reason]);
        }
        NSLog(@"Execution continues!");
    }
    return 0;
}
