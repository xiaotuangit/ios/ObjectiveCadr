//
//  Complex.h
//  Chapter08-02
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Complex : NSObject
{
    double real;
    double imaginary;
}

@property double real, imaginary;

- (void)print;
- (void)setReal:(double)a andImaginary:(double)b;
- (Complex *)add:(Complex *)f;
@end
