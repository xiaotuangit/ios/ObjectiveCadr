//
//  main.m
//  Chapter08-06
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Complex.h"
#import "Fraction.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        id dataValue;   // 定义了一个id类型变量
        Fraction *f1 = [[Fraction alloc]init];
        Complex *c1 = [[Complex alloc]init];
        [f1 setTo:2 over:5];
        [c1 setReal:10.0 andImaginary:2.5];
        // 第一个分数
        dataValue = f1;
        [dataValue print];  // 调用Fraction的print方法
        // 此时dataValue是复数
        dataValue = c1;
        [dataValue print];  // 调用Complex的print方法
    }
    return 0;
}
