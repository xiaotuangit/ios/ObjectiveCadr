//
//  Fraction.h
//  Chapter08-02
//
//  Created by QinTuanye on 2018/8/1.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject
{
    int numerator;
    int denominator;
}

@property int numerator, denominator;

- (Fraction *)initWith:(int)n denominator:(int)d;
- (void)print;
- (void)setTo:(int)n over:(int)d;
- (double)convertToNum;
- (void)reduce;
- (Fraction *)add: (Fraction *)f;
@end
