//
//  main.m
//  Chapter01-01_main
//
//  Created by QinTuanye on 2018/7/27.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

// 定义main方法，作为程序入口
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"Hello Objective-C");    // 执行输出
    }
    return 0;   // 返回结果
}
