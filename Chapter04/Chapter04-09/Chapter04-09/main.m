//
//  main.m
//  Chapter04-09
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator: NSObject {
    double accumulator;
}
// 存储的方法
- (void)setAccumulator:(double)value;
- (void)clear;
- (double)accumulator;
// 运算方法
- (void)add:(double)value;
- (void)subtract:(double)value;
- (void)multiply:(double)value;
- (void)divide:(double)value;
@end
@implementation Calculator

- (void)setAccumulator:(double)value {
    accumulator = value;
}

- (void)clear {
    accumulator = 0;
}

- (double)accumulator {
    return accumulator;
}

- (void)add:(double)value {
    accumulator += value;
}

- (void)subtract:(double)value {
    accumulator -= value;
}

- (void)multiply:(double)value {
    accumulator *= value;
}

- (void)divide:(double)value {
    accumulator /= value;
}
@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        double value1, value2;
        char operator;
        Calculator *deskCalc = [[Calculator alloc]init];
        NSLog(@"输入一个表达式。");
        scanf("%lf %c %lf", &value1, &operator, &value2);
        [deskCalc setAccumulator: value1];
        if (operator == '+') {
            [deskCalc add: value2];
        } else if (operator == '-') {
            [deskCalc subtract: value2];
        } else if (operator == '*') {
            [deskCalc multiply: value2];
        } else if (operator == '/') {
            [deskCalc divide: value2];
        }
        NSLog(@"%.2f", [deskCalc accumulator]);
    }
    return 0;
}
