//
//  main.m
//  Chapter04-01
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int number;
        NSLog(@"Type in your number: ");
        scanf("%i", &number);
        if (number < 0) {
            number = -number;
        }
        NSLog(@"The absolute value is %i", number);
    }
    return 0;
}
