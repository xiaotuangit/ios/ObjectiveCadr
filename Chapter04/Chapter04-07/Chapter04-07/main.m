//
//  main.m
//  Chapter04-07
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int number, sign;
        NSLog(@"Please type in a number: ");
        scanf("%i", &number);
        if (number < 0) {
            sign = -1;
        } else if (number == 0) {
            sign = 0;
        } else {
            sign = 1;
        }
        NSLog(@"Sign = %i", sign);
    }
    return 0;
}
