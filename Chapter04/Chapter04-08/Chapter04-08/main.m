//
//  main.m
//  Chapter04-08
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        char c;
        NSLog(@"Enter a single character；");
        scanf("%c", &c);
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
            NSLog(@"It's an alphabetic character.");
        } else if (c >= '0' && c <= '9') {
            NSLog(@"It's a digit.");
        } else {
            NSLog(@"It's a special character.");
        }
    }
    return 0;
}
