//
//  main.m
//  Chapter04-04
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int number_to_test, remainder;
        NSLog(@"Enter your number to be tested:");
        scanf("%i", &number_to_test);
        remainder = number_to_test % 2;
        if (remainder == 0) {
            NSLog(@"ou.");
        } else {
            NSLog(@"ji.");
        }
    }
    return 0;
}
