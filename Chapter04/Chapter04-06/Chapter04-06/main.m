//
//  main.m
//  Chapter04-06
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int age = 45;
        if (age > 20) {
            NSLog(@"青年人");
        } else if (age > 40 && !(age > 20)) {
            NSLog(@"中年人");
        } else if (age > 60 && !(age > 20) && !(age > 40 && !(age > 20))) {
            NSLog(@"老年人");
        }
    }
    return 0;
}
