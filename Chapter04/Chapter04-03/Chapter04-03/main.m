//
//  main.m
//  Chapter04-03
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int number_to_test, remainder;
        NSLog(@"输入一个数字进行测试");
        scanf("%i", &number_to_test);
        remainder = number_to_test %2;
        if (remainder == 0) {
            NSLog(@"ou");
        }
        if (remainder != 0) {
            NSLog(@"ji");
        }
    }
    return 0;
}
