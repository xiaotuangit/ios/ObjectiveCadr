//
//  main.m
//  Chapter04-05
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int year, rem_4, rem_100, rem_400;
        NSLog(@"输入一个年份： ");
        scanf("%i", &year);
        rem_4 = year % 4;
        rem_100 = year % 100;
        rem_400 = year %400;
        if ((rem_4 == 0 && rem_100 != 0) || rem_400 == 0) {
            NSLog(@"是闰年.");
        } else {
            NSLog(@"不是闰年");
        }
    }
    return 0;
}
