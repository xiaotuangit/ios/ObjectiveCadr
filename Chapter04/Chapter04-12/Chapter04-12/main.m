//
//  main.m
//  Chapter04-12
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        char score = 'C'; // 声明变量score, 并未其赋值为'C'
        // 执行switch分支语句
        switch (score) {
            case 'A':
                NSLog(@"优秀");
                break;
                
            case 'B':
                NSLog(@"良好");
                break;
                
            case 'C':
                NSLog(@"及格");
                break;
                
            case 'D':
                NSLog(@"不及格");
                break;
                
            default:
                NSLog(@"成绩输入错误");
        }
    }
    return 0;
}
