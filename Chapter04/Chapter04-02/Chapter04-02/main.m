//
//  main.m
//  Chapter04-02
//
//  Created by QinTuanye on 2018/3/29.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction: NSObject {
    int numerator;
    double denominator;
}

- (void)print;
- (void)setNumerator:(int)n;
- (void)setDenominator:(int)d;
- (int)numerator;
- (double)denominator;
- (double)convertToNum;
@end

@implementation Fraction

- (void)print{
    NSLog(@"%i/%f", numerator, denominator);
}

- (void)setNumerator:(int)n {
    numerator = n;
}

- (void) setDenominator:(int)d {
    denominator = d;
}

- (int)numerator {
    return numerator;
}

- (double)denominator {
    return denominator;
}

- (double)convertToNum {
    if (denominator != 0) {
        return (double)numerator / denominator;
    } else {
        return 0.0;
    }
}
@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Fraction *aFraction = [[Fraction alloc]init];
        Fraction *bFraction = [[Fraction alloc]init];
        [aFraction setNumerator: 1];    // 第一个是1/4
        [aFraction setDenominator: 4];
        [aFraction print];
        NSLog(@" =");
        NSLog(@"%g", [aFraction convertToNum]);
        [bFraction print];
        NSLog(@" =");
        NSLog(@"%g", [bFraction convertToNum]);
        
    }
    return 0;
}
