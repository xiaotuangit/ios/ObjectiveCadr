//
//  main.m
//  Chapter03-02
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int a = 25;
        int b = 2;
        int result;
        float c = 25.0;
        float d = 2.0;
        
        NSLog(@"6 + a / 5 * b = %i", a + a / 5 * b);
        NSLog(@"a / b * b = %i", a / b * b);
        NSLog(@"c / d * d = %f", c / d * d);
        NSLog(@"-a = %i", -a);
    }
    return 0;
}
