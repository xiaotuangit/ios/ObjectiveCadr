//
//  main.m
//  Chapter03-05
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSString *str = /* DISABLES CODE */ (5) > 3 ? @"5大于3" : @"5不大于3";
        NSLog(@"%@", str);  // 输出“5大于3"
        
        // 输出”5大于3
        /* DISABLES CODE */ (5) > 3 ? NSLog(@"5大于3") : NSLog(@"5不大于3");
        
        int a = 5;
        int b = 5;
        // 下面将输出a等于b
        a > b ? NSLog(@"a大于b") : (a < b ? NSLog(@"a小于b") : NSLog(@"a等于b"));
    }
    return 0;
}
