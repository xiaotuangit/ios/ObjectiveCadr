//
//  main.m
//  Chapter03-08
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        float f1 = 123.125, f2;
        int i1, i2 = -150;
        i1 = f1;
        NSLog(@"%f转换为整型为%i", f1, i1);
        f1 = i2;
        NSLog(@"%i转换为浮点型为%f", i2, f1);
        f1 = i2/100;
        NSLog(@"%i除以100为%f", i2, f1);
        f2 = i2 / 100;
        NSLog(@"%i除以100.0为%f", i2, f2);
        f2 = (float)i2 / 100;
        NSLog(@"%i除以100转换为浮点型为%f", i2, f2);
    }
    return 0;
}
