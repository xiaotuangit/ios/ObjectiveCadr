//
//  main.m
//  Chapter03-14
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int a = 2;  // 定义变量a，将其赋值为2
        // 将a赋值为逗号表达式的值，结果a的值为真（用1表示）
        a = (a *= 3, 5 < 8);
        NSLog(@"%d", a);
        // 对a连续赋值，最后a的值为9， 整个逗号表达是返回9，因此x的值为9
        int x = (a = 3, a = 4, a = 6, a = 9);
        NSLog(@"a: %d, x: %d", a, x);
    }
    return 0;
}
