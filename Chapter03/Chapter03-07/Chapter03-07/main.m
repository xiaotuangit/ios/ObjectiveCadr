//
//  main.m
//  Chapter03-07
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"5是否大于4.0: %d",(5 > 4.0));   // 输出1
        NSLog(@"5和5.0是否相等: %d", (5 == 5.0));    // 输出1
        NSLog(@"97和'a'是否相等: %d", (97 == 'a'));  // 输出1
        NSLog(@"YES和NO是否相等: %d", (YES == NO));  // 输出0
        // 创建两个NSDate对象，分别赋给t1和t2两个引用
        NSDate *t1 = [NSDate date];
        NSDate *t2 = [NSDate date];
        // t1和t2是同一个类的两个实例的引用，所以可以比较，
        // 但t1和t2引用不同的对象，所以返回0
        NSLog(@"t1是否等于t2: %d", (t1 == t2));
    }
    return 0;
}
