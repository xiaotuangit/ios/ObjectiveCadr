//
//  main.m
//  Chapter03-04
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        float f1 = 123.125, f2;
        int i1, i2 = -150;
        i1 = f1;    // float转换成int
        NSLog(@"%f assigned to an int produces %i", f1, i1);
        f1 = i2;    // int 转换成float
        NSLog(@"%i assigned to a float produces %f", i2, f1);
        f2 = i2 / 100;  // int 类型的整除
        NSLog(@"%i divided by 100.0 produces %f", i2, f2);
        f2 = (float) i2 / 100;  // 类型转换操作符
        NSLog(@"(float) %i divided by 100 produces %f", i2, f2);
    }
    return 0;
}
