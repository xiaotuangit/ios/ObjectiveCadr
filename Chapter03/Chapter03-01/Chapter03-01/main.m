//
//  main.m
//  Chapter03-01
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int a = 100;
        int b = 2;
        int c = 20;
        int d = 4;
        int result;
        
        result = a -b;  // 减
        NSLog(@"a - b = %i", result);
        
        result = b * c; // 乘
        NSLog(@"b * c = %i", result);
        
        result = a / c; // 除
        NSLog(@"a / c = %i", result);
        
        result = a + b * c; // 混合运算
        NSLog(@"a + b * c = %i", result);
        
        NSLog(@"a * b + c * d = %i", a * b + c * d);
    }
    return 0;
}
