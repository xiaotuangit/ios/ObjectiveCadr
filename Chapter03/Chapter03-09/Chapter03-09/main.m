//
//  main.m
//  Chapter03-09
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 定义一个short类型变量
        short sValue = 5;
        // 表达式中的sValue将自动提升到int类型，因此下面表达是将输出4
        NSLog(@"%ld", sizeof(sValue - 2));
        // 2.0是浮点数，因此下面的计算结果也是浮点数
        double d = sValue / 2.0;
        NSLog(@"%g", d);
    }
    return 0;
}
