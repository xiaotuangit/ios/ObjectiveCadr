//
//  main.m
//  Chapter03-06
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"%i", 3 > 5);
        NSLog(@"%i", 3 < 5);
        NSLog(@"%i", 3 != 5);
    }
    return 0;
}
