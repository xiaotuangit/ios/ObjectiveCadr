//
//  main.m
//  Chapter03-12
//
//  Created by QinTuanye on 2018/3/28.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        double a = 3.2; // 定义变量a，其初值为3.2
        double b = pow(a, 5);   // 求q的5次方，并将计算结果赋给
        NSLog(@"%g", b);    // 输出b的值
        double c = sqrt(a); // 求a的平方根，并将结果赋给c
        NSLog(@"%g", c);    // 输出c的值。
        double d = arc4random() % 10;   // 获取随机数，返回一个0~10之间的伪随机数
        NSLog(@"随机数：%g", d);    // 输出随机数d的值
        double e = sin(1.57);   // 求1.57的sin函数值：1.57被当成弧度数
        NSLog(@"%g", e);    // 输出接近1
        double x = -5.0;    // 定义double变量x， 其值为-5.0
        x = -x; // 对x求负数，其值变成-5.0
        // x实际的值为5.0，但是使用%g格式则输出5
        NSLog(@"%g", x);
    }
    return 0;
}
